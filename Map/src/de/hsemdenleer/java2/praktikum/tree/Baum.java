package de.hsemdenleer.java2.praktikum.tree;

import java.util.LinkedList;
import java.util.List;

/**
 * BaumLsg repr�sentiert einen Baum. Der Baum ist leer oder enth�lt 
 * lediglich einen Verweis auf seinen (einzigen) Wurzelknoten. 
 * Die ganzen anderen Knoten h�ngen als Nachfolgerknoten direkt 
 * oder indirekt an diesem Wurzelknoten.
 * Die Struktur der Knoten ist in der inneren Klasse Knoten festgelegt. 
 * 
 * @author G�nter Totzauer
 *
 */
public class Baum {
	Knoten wurzel = null;
	
	/**
	 * erzeugt einen leeren Baum ohne Wurzel
	 */
	Baum() {
	}
	
	/**
	 * erzeugt einen Baum, der nur die Wurzel enth�lt
	 * @param label stellt die Nutzlast der Wurzel dar
	 */
	Baum(Object label){
		wurzel = new Knoten(label);
	}
	
	/**
	 * erzeugt einen Baum aus beliebig vielen Unterb�umen
	 * @param label stellt die Nutzlast der neuen Wurzel dar
	 * @param b ist das array der zusammenzuf�genden B�ume
	 */
	Baum(Object label, Baum... b) {
		wurzel = new Knoten(label);
		
		/* neue erweiterte for-Schleife seit Java 5.0
		 * siehe Javabuch unter for-Schleife (erweiterte)
		 */
		for (Baum bb : b) {
			wurzel.list.add(bb.wurzel);
		}
	}
	
	/**
	 * berechnet die H�he des Baumes als die H�he seiner Wurzel.
	 * Ist der Baum leer ist das Ergebnis 0.
	 * @return H�he des Baumes
	 */
	public int hoehe(){
		if (wurzel == null) {
			return 0;
		} 
		return wurzel.hoehe();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		if (wurzel == null) {
			return "<Baum>\n</Baum>\n";
		} else {
			return "<Baum>\n" + wurzel.toStringRek("") + "</Baum>\n";
		}
	}
	
	/**
	 * Repr�sentiert einen Knoten eines Baumes.
	 */
	private class Knoten {
		/**
		 * wird f�r die formatierte rekursive Darstellung verwendet.
		 */
		static final String prefix = "--";
		
		/**
		 * Nutzlast des Knoten
		 */
		Object label;
		
		/**
		 * Liste aller Nachfolgerknoten
		 */
		List<Knoten> list;
		
		/**
		 * erzeugt einen Knoten mit Nutzlast und leerer Nachfolgerliste
		 * @param label Nutzlast des Knoten
		 */
		Knoten(Object label) {
			this.setLabel(label);
			list = new LinkedList<Knoten>();
		}
		
		/**
		 * erzeugt einen Knoten mit Nutzlast und Nachfolgerliste
		 * @param label Nutzlast des Knoten
		 * @param k array der Nachfolgerknoten
		 */
		@SuppressWarnings("unused")
		Knoten(Object label, Knoten... k) {
			this(label);
			/* neue erweiterte for-Schleife seit Java 5.0
			 * siehe Javabuch unter for-Schleife (erweiterte)
			 */
			for (Knoten kk: k) {
				list.add(kk);
			}
		}
		
		/**
		 * setzt die Nutzlast des Knoten
		 * @param label Nutzlast
		 */
		void setLabel(Object label) {
			this.label = label;
		}
		
		/**
		 * @return Nutzlast des Knoten
		 */
		Object getLabel() {
			return label;
		}
		
		/**
		 * @return Hoehe des Knoten
		 */
		public int hoehe(){
			int hoehe = 0;
			int hoehealt = 0;
			for (Knoten k: list) {
				hoehe = k.hoehe();
				if(hoehe > hoehealt)
					hoehealt = hoehe;
			}
			return 1+hoehealt;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return getLabel().toString();
		}
		
		/**
		 * erzeugt rekursiv eine Stringrepr�sentation des Knoten 
		 * und aller seiner Nachfolger.
		 * 
		 * @param s = "" bei direktem Aufruf durch toStringRek()
		 * @return
		 */
		private String toStringRek(String s) {
			String res =  s + getLabel().toString() + " ("+ hoehe() +")\n";
			
			for (Knoten k: list) {
				res += k.toStringRek(s + prefix);
			}
			return res;
		}
		
		/**
		 * erzeugt rekursiv eine Stringrepr�sentation des Knoten 
		 * und aller seiner Nachfolger.
		 
		 * @return Stringrepr�sentation des Knoten 
		 */
		@SuppressWarnings("unused")
		public String toStringRek() {
			return toStringRek("");
		}
	}   // class Knoten
	
	/**
	 * Erzeugung und Ausgabe von Beispielb�umen
	 */
	public static void main(String[] args) {
		Baum b;
		
		b = new Baum(); 
		System.out.println(b);
		
		b = new Baum("Buch" 
				,new Baum("K1"
						,new Baum("K1.1")
						,new Baum("K1.2")
						,new Baum("K1.3")
				)
				,new Baum("K2")
				,new Baum("K3"
						,new Baum("K3.1"
								,new Baum("K3.1.1")
								,new Baum("K3.1.2")
						)
						,new Baum("K3.2")
				)
		);
		System.out.println(b);
		System.out.println(b.hoehe());
	}   // main()
}  // class Baum
