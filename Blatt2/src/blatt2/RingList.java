package blatt2;

import java.util.Iterator;

public class RingList<E> implements List<E> {
	private class Wrapper {
		E e;
		Wrapper succ;
		
		Wrapper(E e){
			this.e = e;
			succ = null;
		}
	}
	
	Wrapper first;
	Wrapper last;
	int size;
	
	RingList(){
		Wrapper n = new Wrapper(null);
		first = n;
		last = n;
		last.succ = first;
		size = 0;
	}

	public boolean add(E e) {
		Wrapper w = new Wrapper(e);
		last.succ = w;
		last = w;
		last.succ = first;
		size++;
		return true;
	}

	public E get(int index) {
		Wrapper wRun;
		if (index < 0 || index >= size){
			throw new IndexOutOfBoundsException();
		}
		wRun = this.first.succ;
		for (int iRun = 0; iRun < index; iRun++){
			wRun = wRun.succ;
		}
		return wRun.e;
	}

	public int size() {
		return size;
	}
	
	E remove (int index) {
		Wrapper wRun;
		Wrapper wDel;
		if (index < 0 || index >= size){
			throw new IndexOutOfBoundsException();
		}
		wRun = this.first.succ;
		for(int iRun = 0; iRun < index-1; iRun++) {
			wRun = wRun.succ;
		}
		wDel = wRun.succ;
		wRun.succ = wDel.succ;
		return wDel.e;
	}
	
	boolean remove(Object o) {
		Wrapper wRun;
		Wrapper wDel;
		wRun = this.first.succ;
		@SuppressWarnings("unchecked")
		E e = (E) o;
		for(int iRun = 0; iRun < size; iRun++) {
			if(wRun.e.equals(e))
			{
				wDel = wRun.succ;
				wRun.succ = wDel.succ;
				return true;
			}
			else
			{
				wRun = wRun.succ;
			}
		}
		
		return false;
	}
	
	int indexOf(Object o) {
		Wrapper wRun;
		
		wRun = this.first.succ;
		@SuppressWarnings("unchecked")
		E e = (E) o;
		for(int iRun = 0; iRun < size; iRun++) {
			if(wRun.e.equals(e))
			{
				return iRun;
			}
			else
			{
				wRun = wRun.succ;
			}
		}

		return -1;
	}

	public Iterator<E> iterator() {
		return new Iterator<E>(){
			Wrapper next = first.succ;
			Wrapper current;

			public boolean hasNext() {
				return next != first;
			}

			public E next() {
				current = next;
				if (next != null){
					next = next.succ;
				}
				return current.e;
			}

			public void remove() {
				throw new UnsupportedOperationException();	
			}
		};
	}
}
