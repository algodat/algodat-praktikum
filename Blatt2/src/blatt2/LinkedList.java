package blatt2;

import java.util.Iterator;

public class LinkedList<E> implements List<E> {
	private class Wrapper {
		E e;
		Wrapper succ;
		
		Wrapper(E e){
			this.e = e;
			succ = null;
		}
	}
	
	Wrapper first;
	Wrapper last;
	int size;
	
	LinkedList(){
		first = null;
		last = null;
		size = 0;
	}

	public boolean add(E e) {
		Wrapper w = new Wrapper(e);
		if (size == 0){
			first = w;
			last = w;
		} else {
			last.succ = w;
			last = w;
		}
		size++;
		return true;
	}

	public E get(int index) {
		Wrapper wRun;
		if (index < 0 || index >= size){
			throw new IndexOutOfBoundsException();
		}
		wRun = this.first;
		for (int iRun = 0; iRun < index; iRun++){
			wRun = wRun.succ;
		}
		return wRun.e;
	}

	public int size() {
		return size;
	}

	public Iterator<E> iterator() {
		return new Iterator<E>(){
			Wrapper next = first;
			Wrapper current;

			public boolean hasNext() {
				return next != null;
			}

			public E next() {
				current = next;
				if (next != null){
					next = next.succ;
				}
				return current.e;
			}

			public void remove() {
				throw new UnsupportedOperationException();	
			}
		};
	}
}
