package blatt2;

import java.util.Iterator;


public class Main {

	public static void main(String[] args) {
		int n = 5;
		if (args.length > 0){
			n = Integer.parseInt(args[0]);
		}
		
//		List<Integer> aList = new LinkedList<Integer>();
//		List<Integer> cList = new ArrayList<Integer>();
		List<Integer> rList = new RingList<>();
		
		for (int i = 0; i < n; i++){
//			aList.add(i*i);
//			cList.add(i*i);
			rList.add(i*i);
		}
		
		/* Ausgabeschleife 1 fuer Integer-Liste aList */
//		for (int i = 0; i < aList.size(); i++){
//			System.out.println(aList.get(i));
//		}
//		System.out.println("=======================");
		
//		for (int i = 0; i < cList.size(); i++){
//			System.out.println(cList.get(i));
//		}
//		System.out.println("=======================");
		
//		for (int val: aList){
//			System.out.println(val);
//		}
//		System.out.println("=======================");
		
		for (int val: rList){
			System.out.println(val);
		}
		System.out.println("=======================");
		
//		List<String> bList = new LinkedList<String>();
//		List<String> dList = new ArrayList<String>();
		List<String> sList = new RingList<String>();
		
//		for (int i = 0; i < n; i++){
//			bList.add("als String " + i * i);
//		}
		
		for (int i = 0; i < n; i++){
			sList.add("als String " + i * i);
		}
		/* Ausgabeschleife 2 fuer String-Liste bList */
//		for (Iterator<String> it = (Iterator<String>) bList.iterator(); it.hasNext();){
//			System.out.println(it.next());
//		}
//		System.out.println("=======================");
		
/*		for (Iterator<String> it = (Iterator<String>) dList.iterator(); it.hasNext();){
			System.out.println(it.next());
		}*/
		for(Iterator<String> it = (Iterator<String>) sList.iterator(); it.hasNext();)
		{
			System.out.println(it.next());
		}
		System.out.println("=======================");
	}
}
