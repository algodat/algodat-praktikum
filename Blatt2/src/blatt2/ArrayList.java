package blatt2;

import java.util.Iterator;

public class ArrayList<E> implements List<E> {
	private static int startSize = 10;
	private E elem[];
	
	int size;
	
	ArrayList()
	{
		size = -1;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean add(E e) {
		if(size == -1)
		{
			elem = (E[]) new Object[startSize];
			size = 0;
		}
		
		if(size+10>=startSize)
		{
			resizeArray(startSize*2);
		}
		
		elem[size] = e;
		size++;
		return true;
	}
	
	private void resizeArray(int newSize)
	{
		@SuppressWarnings("unchecked")
		E elem[] = (E[]) new Object[newSize];
		for(int i = 0; i < size; i++)
		{
			elem[i] = this.elem[i];
		}
		this.elem = elem;
		startSize = newSize;
	}
	
	@Override
	public E get(int index) {
		return elem[index];
	}
	
	public int size() {
		return size;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
}
