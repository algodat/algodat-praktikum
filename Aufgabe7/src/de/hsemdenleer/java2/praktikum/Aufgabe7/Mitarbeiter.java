package de.hsemdenleer.java2.praktikum.Aufgabe7;

public class Mitarbeiter {
	private int personalnummer;
	private String nachname;
	private String vorname;
	private int geburtsjahr;
	private String wohnort;
	private String gehalt;
	private String abteilung;
	
	Mitarbeiter(int personalnummer, String nachname, String vorname, int geburtsjahr, String wohnort, String gehalt, String abteilung)
	{
		this.personalnummer = personalnummer;
		this.nachname = nachname;
		this.vorname = vorname;
		this.geburtsjahr = geburtsjahr;
		this.wohnort = wohnort;
		this.gehalt = gehalt;
		this.abteilung = abteilung;
	}
	
	public Mitarbeiter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Mitarbeiter))
			return false;
		Mitarbeiter m = (Mitarbeiter) obj;
		if(m.getPersonalnummer() == this.personalnummer)
			return true;
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(personalnummer);
		sb.append("\t");
		sb.append(nachname);
		sb.append("\t");
		sb.append(vorname);
		sb.append("\t");
		sb.append(geburtsjahr);
		sb.append("\t");
		sb.append(wohnort);
		sb.append("\t");
		sb.append(gehalt);
		sb.append("\t");
		sb.append(abteilung);
		return sb.toString();
	}
	
	/*
	 * Getter und Setter
	 */
	
		public int getPersonalnummer() {
			return personalnummer;
		}	
		public void setPersonalnummer(int personalnummer) {
			this.personalnummer = personalnummer;
		}
		public String getNachname() {
			return nachname;
		}
		public void setNachname(String nachname) {
			this.nachname = nachname;
		}
		public String getVorname() {
			return vorname;
		}
		public void setVorname(String vorname) {
			this.vorname = vorname;
		}
		public int getGeburtsjahr() {
			return geburtsjahr;
		}
		public void setGeburtsjahr(int geburtsjahr) {
			this.geburtsjahr = geburtsjahr;
		}
		public String getWohnort() {
			return wohnort;
		}
		public void setWohnort(String wohnort) {
			this.wohnort = wohnort;
		}
		public String getGehalt() {
			return gehalt;
		}
		public void setGehalt(String gehalt) {
			this.gehalt = gehalt;
		}
		public String getAbteilung() {
			return abteilung;
		}
		public void setAbteilung(String abteilung) {
		this.abteilung = abteilung;
	}
	
}
