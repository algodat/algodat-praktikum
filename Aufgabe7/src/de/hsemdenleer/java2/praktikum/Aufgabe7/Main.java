package de.hsemdenleer.java2.praktikum.Aufgabe7;

public class Main {

	public static void main(String[] args) {
		Mitarbeiterverwaltung verwaltung = new Mitarbeiterverwaltung();
		
		for(int i = 1; i <= 20; i++)
			verwaltung.add(new Mitarbeiter(i, "Mustermann", "Manuel", 1975+i, (i % 2 == 0) ? "Emden": "Lingen", "5", (i % 2 == 0) ? "S1":"S2"));

		
		for(Mitarbeiter m : verwaltung.lookupGeburtsjahr(1985))
		{
			System.out.println(m.toString());
		}
		
		System.out.println("----");
		
		for(Mitarbeiter m : verwaltung.lookupWohnort("Lingen"))
		{
			System.out.println(m.toString());
		}
		
		System.out.println("----");
		
		for(Mitarbeiter m : verwaltung.lookupAbteilung("S1"))
		{
			System.out.println(m.toString());
		}
		
		System.out.println("----");
		
		System.out.println(verwaltung.lookup(20).toString());
		
		System.out.println("----");
		
		for(int i = 1; i <= 20; i++)
		{
			if(verwaltung.remove(i) == null)
				System.out.println("OmaIsDeadException");
		}
		
	}

}
