package de.hsemdenleer.java2.praktikum.Aufgabe7;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Mitarbeiterverwaltung {
	private HashMap<Integer, Mitarbeiter> mapMitarbeiter = new HashMap<Integer, Mitarbeiter>();
	private HashMap<String, LinkedList<Mitarbeiter>> mapWohnort = new HashMap<String, LinkedList<Mitarbeiter>>();
	private HashMap<String, LinkedList<Mitarbeiter>> mapAbteilung = new HashMap<String, LinkedList<Mitarbeiter>>();
	private HashMap<Integer, LinkedList<Mitarbeiter>> mapGeburtsJahr = new HashMap<Integer, LinkedList<Mitarbeiter>>();
	
	public void add(Mitarbeiter m)
	{
		mapMitarbeiter.put(m.getPersonalnummer(), m);
		if(!mapWohnort.containsKey(m.getWohnort()))
			mapWohnort.put(m.getWohnort(), new LinkedList<Mitarbeiter>());
		if(!mapAbteilung.containsKey(m.getAbteilung()))
			mapAbteilung.put(m.getAbteilung(), new LinkedList<Mitarbeiter>());
		if(!mapGeburtsJahr.containsKey(Integer.valueOf(m.getGeburtsjahr())))
			mapGeburtsJahr.put(Integer.valueOf(m.getGeburtsjahr()), new LinkedList<Mitarbeiter>());
		
		mapWohnort.get(m.getWohnort()).add(m);
		mapAbteilung.get(m.getAbteilung()).add(m);
		mapGeburtsJahr.get(Integer.valueOf(m.getGeburtsjahr())).add(m);
	}
	
	public Mitarbeiter remove(int PersNr)
	{
		Mitarbeiter m = mapMitarbeiter.get(PersNr);
		if(m == null)
		{
			return null;
		}
		mapWohnort.get(m.getWohnort()).remove(m);
		mapAbteilung.get(m.getAbteilung()).remove(m);
		mapGeburtsJahr.get(m.getGeburtsjahr()).remove(m);
		if(mapWohnort.get(m.getWohnort()).isEmpty())
			mapWohnort.remove(m.getWohnort());
		if(mapAbteilung.get(m.getAbteilung()).isEmpty())
			mapAbteilung.remove(m.getAbteilung());
		if(mapGeburtsJahr.get(m.getGeburtsjahr()).isEmpty())
			mapGeburtsJahr.remove(m.getGeburtsjahr());
		return(mapMitarbeiter.remove(Integer.valueOf(PersNr)));
	}
	
	public Mitarbeiter lookup(int PersNr)
	{
		System.out.println("Zugriff auf PersNr:" + PersNr);
		return (mapMitarbeiter.get(PersNr));
	}
	
	public List<Mitarbeiter> lookupWohnort(String wohnort)
	{
		System.out.println("Zugriff auf Wohnort: " + wohnort);
		return (mapWohnort.get(wohnort));
	}
	
	public List<Mitarbeiter> lookupAbteilung(String abtlg)
	{
		System.out.println("Zugriff auf Abteilung: " +abtlg);
		return (mapAbteilung.get(abtlg));
	}
	
	public List<Mitarbeiter> lookupGeburtsjahr(int gebJahr)
	{
		System.out.println("Zugriff auf GebJahr: " + gebJahr);
		return(mapGeburtsJahr.get(Integer.valueOf(gebJahr)));
	}

}
