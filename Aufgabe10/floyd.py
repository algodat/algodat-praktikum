DEBUG = True
def floyd(n,w):
  global DEBUG
  #eigendlicher algo
  d = w
  for k in range(1,n):
    for i in range(len(d)):
      for j in range(len(d[0])):
        d_temp = d[i][j]
        d_neu = d[i][k]+d[k][j]
        d[i][j] = min(d_temp,d_neu)
    if DEBUG:
      print '\n\n\n step '+ str(k)+':'
      for z in range(len(d)):
        print d[z]

  return d
def main():
  inf = float("inf")
  n = 10
  d=[
  [0,1,inf,inf,6,7,inf,inf,inf,8],
  [1,0,inf,inf,3,inf,2,inf,inf,4],
  [inf,inf,0,9,inf,inf,inf,3,5,inf],
  [inf,inf,7,0,inf,inf,inf,6,3,inf],
  [7,inf,inf,inf,0,1,6,inf,inf,inf],
  [inf,4,inf,inf,2,0,inf,inf,inf,3],
  [8,inf,inf,inf,5,2,0,inf,inf,inf],
  [inf,inf,1,4,inf,inf,inf,0,1,inf],
  [inf,inf,2,inf,inf,inf,inf,3,0,inf],
  [inf,3,inf,inf,4,inf,1,inf,inf,0]
  ]
  #ausgabe eingangszustand
  print 'input:'
  for z in range(len(d)):
    print d[z]

  d = floyd(n,d)

  #ausgabe kuerzeste wege
  print '\n\n\noutput:'
  for z in range(len(d)):
    print d[z]
#/main()
main()
