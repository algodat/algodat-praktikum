package de.hsemdenleer.algodat.praktikum.Aufgabe6;

import java.util.Arrays;

public class Heap {
	private int[] data;
	private int swap = 0;
	private int size;
	private int writeIndex = 0;

	public Heap(int n) {
		size = n;
		data = new int[size];
		writeIndex = 0;
	}

	public void insert(int i) {
		data[writeIndex] = i;
		
		
		int readIndex = writeIndex;
		writeIndex++;
		boolean bSorting = true;

		while (bSorting) {
			int parent = (readIndex-1) / 2;
			if (data[readIndex] < data[parent] && parent >= 0) {
				swap(readIndex, parent);
				readIndex = parent;
			} else {
				bSorting = false;
			}
		}
	}

	public void insert(int... i) {
		for (int n = 0; n < size ; n++)
			data[n] = i[n];
		boolean bSort = true;
		while(bSort){
			bSort = false;
			for (int n = ((size-1)/2)-1; n >= 0 ; n--){
				if (bSort == false)
					bSort =  sink(n);
				else
					sink(n);
			}
		}
	}
	
	private boolean sink(int i){
		boolean ret = true;
		int parent = i;
		int leftChild = 2*i+1;
		int rightChild = 2*i+2;
		
		int swapChild = (data[leftChild] <data[rightChild])?leftChild:rightChild;
		
		if(data[parent] > data[swapChild]){
			swap(parent,swapChild);
			ret = true;
		}else{
			ret = false;
		}
		return ret;
	}

	public boolean isHeap() {
		boolean ret = true;
		int len = data.length - 1;
		for (int i = len; i > 0; i--) {

			if (!((2 * i > len) ? true : data[i] <= data[2 * i]
					&& (2 * i - 1 > len) ? true : data[2 * i - 1] >= data[i]))
				ret = false;
		}
		return ret;
	}

	public void swap(int toSink, int toGrow) {
		//System.out.println("to Sink:" + data[toSink] + "\t toGrow:"+ data[toGrow]);
		int temp = data[toSink];
		data[toSink] = data[toGrow];
		data[toGrow] = temp;
		swap++;
	}

	@Override
	public String toString() {
		return (Arrays.toString(data));
	}

	public int getSwap() {
		return swap;
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
}
