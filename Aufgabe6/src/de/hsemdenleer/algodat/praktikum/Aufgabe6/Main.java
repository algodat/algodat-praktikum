package de.hsemdenleer.algodat.praktikum.Aufgabe6;

public class Main {

	public static void main(String[] args) {


		for (int c = 1; c <= 20; c++) {

			int n = (int) (Math.pow(2, c))-1;
			Heap h = new Heap(n);

			for (int i = n; i > 0; i--) {
				h.insert(i);
			}

			System.out.println("===Iterativ===");
			System.out.println("Vertauschungen: " + h.getSwap() + "\n\t\tfür "
					+ n + " Elemente");

			Heap b = new Heap(n);
			int inhalt[] = new int[n];
			for (int i = n; i > 0; i--)
				inhalt[(n - i)] = i;
			b.insert(inhalt);
			System.out.println("===Geblockt===");
//			if (!b.isHeap()) {
//				System.out.println("isHeap(): false");
//				System.out.println(b.toString());
//			} else
//				System.out.println("isHeap(): true");
			System.out.println("Vertauschungen: " + b.getSwap() + "\n\t\tfür "
					+ n + " Elemente");
		}
	} // main()
}
